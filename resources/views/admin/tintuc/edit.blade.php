@extends('admin.layout.index')

@section('title') Sửa tin tức
@endsection

@section('content')
<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Tin tức
                    <small>Sửa</small>
                </h1>
            </div>
            <!-- /.col-lg-12 -->
            <div class="col-lg-12" style="padding-bottom:120px">
                @if(count($errors)>0)
                    <div class="alert alert-danger">
                        @foreach($errors->all() as $err)
                            {{$err}}<br>
                        @endforeach
                    </div>
                @endif

                @if(session('Notified'))
                    <div class="alert alert-success">
                        {{session('Notified')}}
                    </div>
                @endif

                <form action="admin/loaitin/edit/{{$tintuc->id}}" method="POST" enctype="multipart/form-data">
                    <input type="hidden" name="_token" value="{{csrf_token()}}"/>
                    <div class="form-group">
                        <label>Thể Loại</label>
                        <select class="form-control" name="tentheloai" id="idtheloai">
                            @foreach($theloai as $tl)
                                <option value="{{$tl->id}}">{{$tl->Ten}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Loại Tin</label>
                        <select class="form-control" name="tenloaitin" id="idloaitin">
                            @foreach($loaitin as $lt)
                                <option value="{{$lt->Ten}}">{{$lt->Ten}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Tiêu Đề</label>
                        <input class="form-control" name="tentieude" value="{{$tintuc->TieuDe}}"/>
                    </div>
                    <div class="form-group">
                        <label>Tóm Tắt</label>
                        <textarea class="form-control" name="tentomtat" rows="4"/>{{$tintuc->TomTat}}</textarea>
                    </div>
                    <div class="form-group">
                        <label>Nội Dung</label>
                        <textarea id="demo" class="form-control ckeditor" name="tennoidung"
                                  rows="10">{{$tintuc->NoiDung}}</textarea>
                    </div>
                    <div class="form-group">
                        @if(session('Error'))
                            <div class="alert alert-danger">
                                {{session('Error')}}
                            </div>
                        @endif
                        <label>Hình Ảnh</label>
                        <p><img width="200px" src="upload/tintuc/{{$tintuc->Hinh}}"></p>
                        <input type="file" class="form-control" name="tenhinh"/>
                    </div>
                    <div class="form-group">
                        <label>Nổi bật</label>
                        <label class="radio-inline">
                            <input name="tennoibat" value="0"
                                   @if($tintuc->NoiBat == 0)
                                   {{'checked'}}
                                   @endif
                                   type="radio">Không
                        </label>
                        <label class="radio-inline">
                            <input name="tennoibat" value="1"
                                   @if($tintuc->NoiBat == 1)
                                   {{'checked'}}
                                   @endif
                                   type="radio">Có
                        </label>
                    </div>
                    <button type="submit" class="btn btn-default">Edit</button>
                    <button type="reset" class="btn btn-default">Làm mới</button>
                </form>
            </div>
        </div>
        <!-- /.row -->
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Comments
                    <small>Danh sách</small>
                </h1>
            </div>
        </div>

        @if(session('Notified'))
            <div class="alert alert-success">
                {{session('Notified')}}
            </div>
            @endif
                    <!-- /.col-lg-12 -->
            <table class="table table-striped table-bordered table-hover" id="commentdataTables">
                <thead>
                <tr align="center">
                    <th>ID</th>
                    <th>Username</th>
                    <th>Comments</th>
                    <th>Date</th>
                    <th>Delete</th>
                </tr>
                </thead>
                <tbody>

                @foreach($comment as $cmt)
                    @if($tintuc->id == $cmt->idTinTuc)
                        <tr class="odd gradeX" align="center">
                            <td>{{$cmt->id}}</td>
                            <td>{{$cmt->User->name}}</td>
                            <td>{{$cmt->NoiDung}}</td>
                            <td>{{$cmt->created_at}}</td>
                            <td class="center"><i class="fa fa-trash-o  fa-fw"></i><a
                                        href="admin/comment/delete/{{$cmt->id}}"> Delete</a></td>
                        </tr>
                    @endif
                @endforeach
                </tbody>
            </table>
    </div>
    <!-- /.row -->
</div>
<!-- /.container-fluid -->
@endsection

@section('script')
    <script>
        $(document).ready(function () {
            $('#idtheloai').change(function () {
                var idTheLoai = $(this).val();
                $.get("admin/ajax/loaitin/" + idTheLoai, function (data) {
                    $('#idloaitin').html(data);
                });
            });
        });
    </script>
@endsection