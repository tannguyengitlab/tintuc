@extends('admin.layout.index')

@section('title')Danh sách Slide
@endsection
@section('content')
        <!-- Page Content -->
<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Slide
                    <small>List</small>
                </h1>
            </div>
        </div>
        <!-- /. row -->
        <div class="col-lg-12">
            <!-- Trigger the modal with a button -->
            <div align="right">
                <button type="button" class="btn btn-add btn-success" data-toggle="modal"><i class="fa fa-plus "></i>
                </button>
            </div>
        </div>
        <!-- /.button -->
        <div class="modal" tabindex="-1" role="dialog" id="slideModal">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <form method="POST" id="slideFormModal" enctype="multipart/form-data">
                        @csrf
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            <h4 class="modal-title" align="center"></h4>
                        </div>
                        <!-- /.modal-header -->
                        <div class="col-lg-12">
                            <br/>
                            <div class="alert alert-danger" style="display:none"></div>
                            <div class="alert alert-success" style="display:none"></div>
                        </div>
                        <!-- /.alert-->
                        <div class="modal-body">
                            <div class="row">
                                <div class="form-group col-md-12">
                                    <input type="hidden" class="form-control" name="slideId">
                                    <label>Tên</label>
                                    <input type="text" class="form-control" name="slideName"
                                           placeholder="Nhập tên slide">
                                </div>
                                <div class="form-group col-md-12">
                                    <label>Nội Dung</label>
                                    <input type="text" class="form-control" name="slideContent"
                                           placeholder="Nhập nội dung silde">
                                </div>
                                <div class="form-group col-md-12">
                                    <label>Hình</label></br>
                                    <img id="slideImagePre" width="200px">
                                    <input type="file" class="form-control" name="slideImage">
                                </div>
                                <div class="form-group col-md-12">
                                    <label>Link</label>
                                    <input type="text" class="form-control" name="slideLink"
                                           placeholder="Nhập link">
                                </div>
                            </div>
                            <!-- /.modal-body -->
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Đóng
                                </button>
                                <button class="btn btn-success" id="slideSave">Lưu</button>
                            </div>
                        </div>
                        <!-- /.modal-footer -->
                    </form>
                    <!-- /.form modal -->
                </div>
                {{--</div>--}}
            </div>
        </div>
        <!-- /.Modal -->
        <table class="table table-striped table-bordered table-hover" id="slideDataTables">
            <thead>
            <tr align="center">
                <th>ID</th>
                <th>Tên</th>
                <th>Nội Dung</th>
                <th>Hình</th>
                <th>Link</th>
                <th>Tác Vụ</th>
            </tr>
            </thead>
        </table>
    </div>
    <!-- /.row -->
</div>
<!-- /.container-fluid -->
</div>
<!-- /#page-wrapper -->
@endsection

@section('script')
    <script type="text/javascript" language="javascript">
        var dataTables = getData();
        $(document).ready(function () {
            addData();
            saveData();
            editData();
            deleteData();
        })

        function getData() {
            var data = $('#slideDataTables').DataTable({
                "responsive": true,
                "ajax": "{{ route('slide.getdata') }}",
                "columns": [
                    {"class": "text-center", "data": "id"},
                    {"class": "text-center", "data": "Ten"},
                    {"class": "text-center", "data": "NoiDung"},
                    {
                        "class": "text-center", "data": function (row, type, set, meta) {
                        if (row.Hinh != '') {
                            var url = "upload/slide/" + row.Hinh;
                            return "<img src='" + url + "' style='width: 180px; height: 60px' />"
                        }
                        return '';
                    }
                    },
                    {"class": "text-center", "data": "Link"},
                    {
                        "class": "text-center",
                        "data": function () {
                            var html = '<a class="btn btn-primary btn-edit">' +
                                    '<i class="fa fa-pencil fa-fw"></i></a>';
                            html += " ";
                            html += '<a class="btn btn-danger btn-delete" ><i class="fa fa-trash-o fa-fw"></i></a>';
                            return html;
                        }
                    },
                ]
            });
            return data;
        }

        function saveData() {
            $(document).on('click', '#slideSave', function (e) {
                e.preventDefault();
                var formData = new FormData($('#slideFormModal')[0]);
                $.ajax({
                    url: "{{ route('slide.postsave') }}",
                    method: "POST",
                    data: formData,
                    processData: false,
                    contentType: false,
                    cache: false,
                    success: function (data) {
                        if (data.errors) {
                            $('.alert-danger').html('');
                            $('.alert-danger').fadeIn(2000);
                            $.each(data.errors, function (key, value) {
                                $('.alert-danger').append('<li>' + value + '</li>');
                            });
                            $('.alert-danger').fadeOut(2000);
                        }
                        else {
                            $('.alert-danger').hide();
                            $('#slideFormModal')[0].reset();
                            $('#slideModal').modal('hide');
                            $('#slideDataTables').DataTable().ajax.reload();
                        }
                    }
                })
            })
        }


        function addData() {
            $(document).on('click', '.btn-add', function () {
                $("#slideModal").find("h4").text("Thêm slide");
                $("#slideModal").modal('show');
                $("input[name='slideName']").val('');
                $("input[name='slideContent']").val('');
                document.getElementById("slideImagePre").src = '';
                $("input[name='slideLink']").val('');
            })
        }

        function editData() {
            $(document).on('click', '.btn-edit', function () {
                $("#slideModal").find("h4").text("Sửa slide");
                $("#slideModal").modal('show');
                var row = $(this).parents('tr');
                var data = dataTables.row(row).data();
                $("input[name='slideId']").val(data.id);
                $("input[name='slideName']").val(data.Ten);
                $("input[name='slideContent']").val(data.NoiDung);
                $("input[name='slideLink']").val(data.Link);
                document.getElementById("slideImagePre").src = "upload/slide/" + data.Hinh;
            })
        }

        function deleteData() {
            $(document).on('click', '.btn-delete', function () {
                var row = $(this).parents('tr');
                var data = dataTables.row(row).data();
                var id = data['id'];
                $.ajax({
                    url: "{{ route('slide.postdelete') }}",
                    method: "POST",
                    dataType: "json",
                    data: {
                        id: id,
                    },
                    success: function (data) {
                        $('#slideDataTables').DataTable().ajax.reload();
                    }
                })
            })
        }
    </script>
@endsection