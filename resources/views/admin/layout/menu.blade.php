<div class="navbar-default sidebar" role="navigation">
    <div class="sidebar-nav navbar-collapse">
        <ul class="nav" id="side-menu">
            <li class="sidebar-search">
                <div class="input-group custom-search-form">
                    <input type="text" class="form-control" placeholder="Search...">
                                <span class="input-group-btn">
                                    <button class="btn btn-default" type="button">
                                        <i class="fa fa-search"></i>
                                    </button>
                                </span>
                </div>
                <!-- /input-group -->
            </li>
            <li>
                <a href="admin/dashboard"><i class="fa fa-dashboard fa-fw"></i> Dashboard </a>
            </li>
            <li>
                <a href="admin/theloai/list"><i class="fa fa-folder-open-o fa-fw"></i> Thể loại <span class="fa arrow"></span></a>
            </li>
            <li>
                <a href="admin/loaitin/list"><i class="fa fa-navicon fa-fw"></i> Loại Tin <span class="fa arrow"></span></a>
            </li>
            <li>
                <a href="admin/slide/list"><i class="fa fa-navicon fa-fw"></i> Slide <span class="fa arrow"></span></a>
            </li>
            <li>
                <a href="admin/tintuc/list"><i class="fa fa-rss-square fa-fw"></i> Tin tức <span class="fa arrow"></span></a>
                <ul class="nav nav-second-level">
                    <li>
                        <a href="admin/tintuc/list">Danh sách</a>
                    </li>
                    <li>
                        <a href="admin/tintuc/add">Thêm</a>
                    </li>
                </ul>
                <!-- /.nav-second-level --></li>
            <li>
            <li>
                <a href="admin/User/list"><i class="fa fa-users fa-fw"></i> User<span class="fa arrow"></span></a>
            </li>
        </ul>
    </div>
    <!-- /.sidebar-collapse -->
</div>