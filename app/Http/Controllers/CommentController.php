<?php

namespace App\Http\Controllers;

use App\comment;
use App\tintuc;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CommentController extends Controller
{
    public function getDelete($id)
    {
        $comment = comment::find($id);
        $comment->delete();

        return redirect('admin/tintuc/edit/'.$comment->idTinTuc)->with('Notified','Đã xóa bình luận với ID là '.$id);
    }

    public function postComment(Request $rq, $id)
    {
        $idTinTuc = $id;
        $tintuc = tintuc::find($id);
        $comment = new comment();
        $comment->idTinTuc = $idTinTuc;
        $comment->idUser = Auth::user()->id;
        $comment->NoiDung = $rq->NoiDung;
        $comment->save();
        return redirect('tintuc/'.$id.'/'.$tintuc->TieuDeKhongDau.'.html');
    }
}
