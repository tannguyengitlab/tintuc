@extends('admin.layout.index')
@section('content')

<!-- Page Content -->
<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Users
                    <small>Edit</small>
                </h1>
            </div>
            <!-- /.col-lg-12 -->
            <div class="col-lg-7" style="padding-bottom:120px">
                @if(count($errors)>0)
                    <div class="alert alert-danger">
                        @foreach($errors->all() as $err)
                            {{$err}}<br>
                        @endforeach
                    </div>
                @endif

                @if(session('Notified'))
                    <div class="alert alert-success">
                        {{session('Notified')}}
                    </div>
                @endif
                <form action="admin/User/edit/{{$users->id}}" method="POST">
                    <input type="hidden" name="_token" value="{{csrf_token()}}"/>
                    <div class="form-group">
                        <label>Username</label>
                        <input readonly class="form-control" name="tenuser" value="{{$users->name}}" />
                    </div>
                    @if(session('Error'))
                        <div class="alert alert-danger">
                            {{session('Error')}}
                        </div>
                    @endif
                    <div class="form-group">
                        <input type="checkbox" id="changePassword" class="" name="changePassword"> <label>Đổi mật khẩu</label> </input>
                    </div>
                    {{--<div class="form-group">
                        <label>Password old</label>
                        <input class="form-control" name="tenpassold" />
                    </div>--}}
                    <div class="form-group">
                        <label>Password new</label>
                        <input disabled="" class="form-control password" name="tenpass" />
                    </div>
                    <div class="form-group">
                        <label>Password Repeat</label>
                        <input disabled="" class="form-control password" name="tenpassrepeat" />
                    </div>
                    <div class="form-group">
                        <label>Email</label>
                        <input readonly class="form-control" name="tenemail" value="{{$users->email}}" />
                    </div>
                    <div class="form-group">
                        <label>Level</label>
                        <p>
                            <label class="radio-inline">
                                <input name="tenlevel" value="1"
                                       @if($users->level == 1)
                                       {{'checked'}}
                                       @endif
                                type="radio">Admin
                            </label>
                            <label class="radio-inline">
                                <input name="tenlevel" value="2"
                                       @if($users->level == 2)
                                       {{'checked'}}
                                       @endif
                                       type="radio">User
                            </label>
                        </p>
                    </div>
                    <button type="submit" class="btn btn-default">Edit</button>
                    <button type="reset" class="btn btn-default">Reset</button>
                </form>
            </div>
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
</div>
<!-- /#page-wrapper -->

@endsection

@section('script')
    <script>
        $(document).ready(function()
        {
            $('#changePassword').change(function()
            {
                if($(this).is(":checked"))
                {
                    $(".password").removeAttr('disabled');
                }
                else
                {
                    $(".password").attr('disabled','');
                }
            });
        });
    </script>
@endsection