<?php

namespace App\Http\Controllers;

use App\User;
use Yajra\Datatables\Datatables;
use Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function getList()
    {
        return view('admin.User.list');
    }

    public function getData()
    {
        $user = User::all();
        foreach ($user as $key => $urs) {
            if ($urs->level == 1) {
                $user[$key]['quyen'] = "Admin";
            } else {
                $user[$key]['quyen'] = "User";
            }
        }
        return DataTables::of($user)->toJson();
    }

    public function postSave(Request $rq)
    {
        $validator = Validator::make($rq->all(), [
            'userImage' => 'required|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ],
            [
                'userImage.required' => 'Vui lòng nhập hình ảnh',
                'userImage.mimes' => 'Chỉ chọn file jpeg, png, jpg, gif, svg',
                'userImage.max' => 'File tối đa 2MB'
            ]);

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()->all()]);
        }

        if($rq->userId){
            $user = User::find($rq->userId);
            $user->name = $rq->userName;
            $user->password = bcrypt($rq->userPassword);
            $user->email = $rq->userEmail;
            $user->level = $rq->userLevel;
            if ($rq->hasFile('userImage')) {
                $file = $rq->file('userImage');
                $name = $file->getClientOriginalName();
                $namephoto = time() . '_' . $name;
                $file->move('upload/user', $namephoto);
                $user->Image = $namephoto;
            } else {
                $user->Image = "";
            }
            $user->save();
            return response()->json(['success' => 'Sửa thành công']);
        }
        else {
            $user = new User;
            $user->name = $rq->userName;
            $user->password = bcrypt($rq->userPassword);
            $user->email = $rq->userEmail;
            $user->level = $rq->userLevel;
            if ($rq->hasFile('userImage')) {
                $file = $rq->file('userImage');
                $name = $file->getClientOriginalName();
                $namephoto = time() . '_' . $name;
                $file->move('upload/user', $namephoto);
                $user->Image = $namephoto;
            } else {
                $user->Image = "";
            }
            $user->save();
            return response()->json(['success' => 'Thêm thành công']);
        }
    }

    public function postEdit(Request $rq)
    {
        /*$validator = Validator::make($rq->all(), [
            'name' => 'required',
            'password' => 'required'
        ]);
        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()->all()]);
        }*/

    }

    public function postDelete(Request $rq)
    {
        $users = User::find($rq->id);
        $users->delete();
        return response()->json(['success' => 'Delete successfully']);
    }

    public function getLogin()
    {
        return view('admin/login');
    }

    public function postLogin(Request $rq)
    {
        if (Auth::attempt(['email' => $rq->email, 'password' => $rq->password])) {
            return redirect('admin/theloai/list');
        } else {
            return redirect('admin/login')->with('Notified', 'Email or password incorrect');
        }

    }

    public function getLogout()
    {
        Auth::logout();
        return redirect('admin/login');
    }

}
