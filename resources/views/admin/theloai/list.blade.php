@extends('admin.layout.index')

@section('title')
    Danh sách thể loại
@endsection

@section('content')
    <div id="page-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Thể loại
                        <small>Danh sách</small>
                    </h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="col-lg-12">
                <!-- Trigger the modal with a button -->
                <div align="right">
                    <button type="button" class="btn btn-add btn-success" data-toggle="modal" id="theloaiAdd"><i
                                class="fa fa-plus "></i>
                    </button>
                </div>
            </div>
            <!-- /.button -->
            <div class="modal" tabindex="-1" role="dialog" id="theloaiModal">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <form id="theloaiFormModal">
                            @csrf
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                                <h4 class="modal-title" align="center"></h4>
                            </div>
                            <!-- /.modal-header -->
                            <div class="col-lg-12">
                                <br/>
                                <div class="alert alert-danger" style="display:none"></div>
                                <div class="alert alert-success" style="display:none"></div>
                            </div>
                            <!-- /.alert-->
                            <div class="modal-body">
                                <div class="row">
                                    <div class="form-group col-md-12">
                                        <input type="hidden" class="form-control" name="theloaiId">
                                        <label>Tên thể loại</label>
                                        <input type="text" class="form-control" name="theloaiName"
                                               placeholder="Nhập tên thể loại">
                                    </div>
                                </div>
                                <!-- /.modal-body -->
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Đóng
                                    </button>
                                    <button type="submit" class="btn btn-success" id="theloaiSave">Lưu</button>
                                </div>
                            </div>
                            <!-- /.modal-footer -->
                        </form>
                        <!-- /.form modal -->
                    </div>
                    {{--</div>--}}
                </div>
            </div>
            <!-- /.Modal-->
            <table class="table table-striped table-bordered table-hover" id="theloaiDataTables" width="100%">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>Tên</th>
                    <th>Tên không dấu</th>
                    <th>Tác vụ</th>
                </tr>
                </thead>
            </table>
        </div>
        <!-- /.container-fluid -->
    </div>
    <!-- ./page-wraper -->
@endsection
@section('script')
    <script type="text/javascript" language="javascript">
        var dataTables = getData();
        $(document).ready(function () {
            addData();
            editData();
            saveData();
            deleteData();
        })
        function getData() {
            var data = $('#theloaiDataTables').DataTable({
                "responsive": true,
                "ajax": "{{ route('theloai.getdata') }}",
                "columns": [
                    {"class": "text-center", "data": "id"},
                    {"class": "text-center", "data": "Ten"},
                    {"class": "text-center", "data": "TenKhongDau"},
                    {
                        "class": "text-center",
                        "data": function () {
                            var html = '<a class="btn btn-primary btn-edit">' +
                                    '<i class="fa fa-pencil fa-fw"></i></a>';
                            html += " ";
                            html += '<a class="btn btn-danger btn-delete" ><i class="fa fa-trash-o fa-fw"></i></a>';
                            return html;
                        }
                    },
                ]
            });
            return data;
        }

        function saveData() {
            $(document).on('click', '#theloaiSave', function (e) {
                e.preventDefault();
                var formData = new FormData($('#theloaiFormModal')[0]);
                $.ajax({
                    url: "{{ route('theloai.postsave') }}",
                    type: "POST",
                    data: formData,
                    processData: false,
                    contentType: false,
                    success: function (data) {
                        if (data.errors) {
                            $('.alert-danger').html('');
                            $('.alert-danger').fadeIn(2000);
                            $.each(data.errors, function (key, value) {
                                $('.alert-danger').append('<li>' + value + '</li>');
                            });
                            $('.alert-danger').fadeOut(2000);
                        }
                        else {
                            $('.alert-danger').hide();
                            $('#theloaiFormModal')[0].reset();
                            $('#theloaiModal').modal('hide');
                            $('#theloaiDataTables').DataTable().ajax.reload();
                        }
                    }
                })
            });
        }

        function addData() {
            $(document).on('click', '.btn-add', function (e) {
                e.preventDefault();
                $("#theloaiModal").find("h4").text("Thêm thể loại");
                $("#theloaiModal").modal('show');
                $("input[name='theloaiId']").val('');
                $("input[name='theloaiName']").val('');
            })
        }
        function editData() {
            $(document).on('click', '.btn-edit', function (e) {
                e.preventDefault();
                $("#theloaiModal").find("h4").text("Sửa thể loại");
                $("#theloaiModal").modal('show');
                var row = $(this).parents('tr');
                var data = dataTables.row(row).data();
                $("input[name='theloaiId']").val(data.id);
                $("input[name='theloaiName']").val(data.Ten);
            })
        }

        function deleteData() {
            $(document).on('click', '.btn-delete', function () {
                var row = $(this).parents('tr');
                var data = dataTables.row(row).data();
                var id = data['id'];
                $.ajax({
                    url: "{{ route('theloai.postdelete') }}",
                    method: "POST",
                    dataType: "json",
                    data: {
                        id: id,
                    },
                    success: function (data) {
                        $('#theloaiDataTables').DataTable().ajax.reload();
                    }
                })
            })
        }
    </script>
@endsection
