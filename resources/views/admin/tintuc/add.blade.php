@extends('admin.layout.index')

@section('title') Thêm tin tức
@endsection

@section('content')
<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Tin tức
                    <small>Thêm</small>
                </h1>
            </div>
            <!-- /.col-lg-12 -->
            <div class="col-lg-12" style="padding-bottom:120px">
                @if(count($errors)>0)
                    <div class="alert alert-danger">
                        @foreach($errors->all() as $err)
                            {{$err}}<br>
                        @endforeach
                    </div>
                @endif

                @if(session('Notified'))
                    <div class="alert alert-success">
                        {{session('Notified')}}
                    </div>
                @endif

                <form action="admin/tintuc/add" method="POST" enctype="multipart/form-data">
                    <input type="hidden" name="_token" value="{{csrf_token()}}"/>
                    <div class="form-group">
                        <label>Thể Loại</label>
                        <select class="form-control" name="tentheloai" id="idtheloai">
                            @foreach($theloai as $tl)
                                <option value="{{$tl->id}}">{{$tl->Ten}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Loại Tin</label>
                        <select class="form-control" name="tenloaitin" id="idloaitin">
                            @foreach($loaitin as $lt)
                                <option value="{{$lt->id}}">{{$lt->Ten}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Tiêu Đề</label>
                        <input class="form-control" name="tentieude" placeholder="Nhập tiêu đề"/>
                    </div>
                    <div class="form-group">
                        <label>Tóm Tắt</label>
                        <textarea class="form-control" name="tentomtat" rows="4"/></textarea>
                    </div>
                    <div class="form-group">
                        <label>Nội Dung</label>
                        <textarea id="demo" class="form-control ckeditor" name="tennoidung" rows="10"></textarea>
                    </div>
                    <div class="form-group">
                        @if(session('Error'))
                            <div class="alert alert-danger">
                                {{session('Error')}}
                            </div>
                        @endif
                        <label>Hình Ảnh</label>
                        <input type="file" class="form-control" name="tenhinh"/>
                    </div>
                    <div class="form-group">
                        <label>Nổi bật</label>
                        <label class="radio-inline">
                            <input name="tennoibat" value="0" checked="" type="radio">Không
                        </label>
                        <label class="radio-inline">
                            <input name="tennoibat" value="1" type="radio">Có
                        </label>
                    </div>
                    <button type="submit" class="btn btn-default">Thêm</button>
                    <button type="reset" class="btn btn-default">Làm mới</button>
                </form>
            </div>
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
</div>
<!-- /#page-wrapper -->

@endsection

@section('script')
    <script>
        $(document).ready(function () {
            $('#idtheloai').change(function () {
                var idTheLoai = $(this).val();
                $.get("admin/ajax/loaitin/" + idTheLoai, function (data) {
                    $('#idloaitin').html(data);
                });
            });
        });
    </script>
@endsection