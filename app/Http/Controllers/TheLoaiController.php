<?php

namespace App\Http\Controllers;

use App\theloai;
use Yajra\Datatables\Datatables;
use Validator;
use Illuminate\Http\Request;

class TheLoaiController extends Controller
{
    public function getList()
    {
        return view('admin.theloai.list');
    }

    public function getData()
    {
        $theloai = theloai::all();
        return DataTables::of($theloai)->toJson();
    }

    public function postSave(Request $rq)
    {
        $validator = Validator::make($rq->all(),
            [
                'theloaiName' => 'required|unique:theloais,Ten',
            ],
            [
                'theloaiName.required' => 'Vui lòng nhập tên thể loại',
                'theloaiName.unique' => 'Tên thể loại đã tồn tại'
            ]
        );

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()->all()]);
        }
        if ($rq->theloaiId){
            $theloai = theloai::find($rq->theloaiId);
            $theloai->Ten = $rq->theloaiName;
            $theloai->TenKhongDau = changeTitle($rq->theloaiName);
            $theloai->save();
            return response()->json(['success' => "Cập nhật thành công"]);
        }
        else {
            $theloai = new theloai();
            $theloai->Ten = $rq->theloaiName;
            $theloai->TenKhongDau = changeTitle($rq->theloaiName);
            $theloai->save();
            return response()->json(['success' => 'Tạo mới thành công']);
        }
        return response()->json(['errors' => 'Có lỗi xảy ra']);
    }

    public function postDelete(Request $rq)
    {
        $theloai = theloai::find($rq->id);
        $theloai->delete();
        return response()->json(['success' => "Xóa thành công"]);
    }
}
