@extends('layout.index')

@section('title')
    {{$tintuc->TieuDe}}
@endsection

@section('content')
    <div class="container">
        <div class="row">

            <!-- Blog Post Content Column -->
            <div class="col-lg-9">

                <!-- Blog Post -->

                <!-- Title -->
                <h1>{{$tintuc->TieuDe}}</h1>

                <!-- Author -->
                <p class="lead">
                    by <a href="#">tannguyen</a>
                </p>

                <!-- Preview Image -->
                <img class="img-responsive" src="upload/tintuc/{{$tintuc->Hinh}}" alt="">

                <!-- Date/Time -->
                <p><span class="glyphicon glyphicon-time"></span> Posted on {{$tintuc->created_at}}</p>
                <hr>

                <!-- Post Content -->
                <p class="lead">{{$tintuc->TomTat}}</p>
                <p>{!!$tintuc->NoiDung!!}</p>
                <hr>

                <!-- Blog Comments -->

                <!-- Comments Form -->

                @if(Auth::check())
                <div class="well">
                    <h4>Viết bình luận ...<span class="glyphicon glyphicon-pencil"></span></h4>
                    <form action="comment/{{$tintuc->id}}" method="post" role="form">
                        <input type="hidden" name="_token" value="{{csrf_token()}}">
                        <div class="form-group">
                            <textarea name="NoiDung" class="form-control" rows="3"></textarea>
                        </div>
                        <button type="submit" class="btn btn-primary">Gửi</button>
                    </form>
                </div>

                <hr>
                @endif
                <!-- Posted Comments -->

                <!-- Comment -->
                @foreach($tintuc->comment as $cmt)
                <div class="media">
                    <a class="pull-left" href="#">
                        <img class="media-object" src="upload/user/{{$cmt->User->image}}" width="40px">
                    </a>
                    <div class="media-body">
                        <h4 class="media-heading">{{$cmt->User->name}}
                            <small>{{$cmt->created_at}}</small>
                        </h4>
                        {{$cmt->NoiDung}}
                    </div>
                </div>
                @endforeach

            </div>

            <!-- Blog Sidebar Widgets Column -->
            <div class="col-md-3">

                <div class="panel panel-default">
                    <div class="panel-heading"><b>Tin liên quan</b></div>
                    <div class="panel-body">

                        <!-- item -->
                        @foreach($tinlienquan as $lienquan)
                        <div class="row" style="margin-top: 10px;">
                            <div class="col-md-5">
                                <a href="detail.html">
                                    <img class="img-responsive" src="upload/tintuc/{{$lienquan->Hinh}}" alt="">
                                </a>
                            </div>
                            <div class="col-md-7">
                                <a href="tintuc/{{$lienquan->id}}/{{$lienquan->TieuDeKhongDau}}.html"><b>{{$lienquan->TieuDe}}</b></a>
                            </div>
                            {{--<p>{{$lienquan->TomTat}}</p>--}}
                            <div class="break"></div>
                        </div>
                        @endforeach
                        <!-- end item -->
                    </div>
                </div>

                <div class="panel panel-default">
                    <div class="panel-heading"><b>Tin nổi bật</b></div>
                    <div class="panel-body">

                        <!-- item -->
                        @foreach($tinnoibat as $noibat)
                        <div class="row" style="margin-top: 10px;">
                            <div class="col-md-5">
                                <a href="#">
                                    <img class="img-responsive" src="upload/tintuc/{{$noibat->Hinh}}" alt="">
                                </a>
                            </div>
                            <div class="col-md-7">
                                <a href="tintuc/{{$noibat->id}}/{{$noibat->TieuDeKhongDau}}.html"><b>{{$noibat->TieuDe}}</b></a>
                            </div>
{{--                            <p>{{$noibat->TomTat}}</p>--}}
                            <div class="break"></div>
                        </div>
                        @endforeach
                        <!-- end item -->
                    </div>
                </div>

            </div>

        </div>
        <!-- /.row -->
    </div>
@endsection