@extends('admin.layout.index')

@section('title') Danh sách tin tức
@endsection

@section('content')
        <!-- Page Content -->
<div id="page-wrapper" xmlns="http://www.w3.org/1999/html">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Tin tức
                    <small>Danh sách</small>
                </h1>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
        <div class="col-lg-12">
            <!-- Trigger the modal with a button -->
            <div align="right">
                <button type="button" class="btn btn-add btn-success" data-toggle="modal"><i class="fa fa-plus "></i>
                </button>
            </div>
        </div>
        <!-- /.button -->
        <div class="modal" tabindex="-1" role="dialog" id="tintucModal">
            <div class="modal-dialog" role="document" style="width:90%;">
                <div class="modal-content">
                    <form method="POST" id="tintucFormModal" enctype="multipart/form-data">
                        @csrf
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            <h4 class="modal-title" align="center"></h4>
                        </div>
                        <!-- /.modal-header -->
                        <div class="col-lg-12">
                            <br/>
                            <div class="alert alert-danger" style="display:none"></div>
                            <div class="alert alert-success" style="display:none"></div>
                        </div>
                        <!-- /.alert-->
                        <div class="modal-body">
                            <div class="row">
                                <div class="form-group col-md-12">
                                    <label>Thể Loại</label>
                                    <select class="form-control" name="tintucTheLoai" id="tintucidTheLoai">
                                        @foreach($theloai as $tl)
                                            <option value="{{$tl->id}}">{{$tl->Ten}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group col-md-12">
                                    <label>Loại Tin</label>
                                    <select class="form-control" name="tintucLoaiTin" id="tintucidLoaiTin">
                                        @foreach($loaitin as $lt)
                                            <option value="{{$lt->id}}">{{$lt->Ten}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group col-md-12">
                                    <input type="hidden" class="form-control" name="tintucId">
                                    <label>Tiêu đề</label>
                                    <input type="text" class="form-control" name="tintucTieuDe"
                                           placeholder="Nhập tiêu đề tin tức">
                                </div>
                                <div class="form-group col-md-12">
                                    <label>Tóm tắt</label>
                                    <textarea class="form-control" name="tintucTomTat" rows="3"/></textarea>
                                </div>
                                <div class="form-group col-md-12">
                                    <label>Nội Dung</label>
                                    <textarea id="tintucNoiDung" class="form-control ckeditor" name="tintucNoiDung"
                                              rows="8"/></textarea>
                                </div>
                                <div class="form-group col-md-12">
                                    <label>Hình</label></br>
                                    <img id="tintucImagePre" width="200px">
                                    <input type="file" class="form-control" name="tintucImage">
                                </div>
                                <div class="form-group col-md-12">
                                    <label>Nổi bật</label>
                                    <label class="radio-inline">
                                        <input name="tintucNoiBat" value="0" checked="" type="radio">Không
                                    </label>
                                    <label class="radio-inline">
                                        <input name="tintucNoiBat" value="1" type="radio">Có
                                    </label>
                                </div>
                            </div>
                            <!-- /.modal-body -->
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Đóng
                                </button>
                                <button class="btn btn-success" id="tintucSave">Lưu</button>
                            </div>
                        </div>
                        <!-- /.modal-footer -->
                    </form>
                    <!-- /.form modal -->
                </div>
                {{--</div>--}}
            </div>
        </div>
        <!-- /.Modal -->
        <table class="table table-striped table-bordered table-hover" id="tintucDataTables" width="100%">
            <thead>
            <tr align="center">
                <th>ID</th>
                <th>Tiêu Đề</th>
                {{--<th>Tóm Tắt</th>--}}
                <th>Hình</th>
                <th>Nổi Bật</th>
                <th>Lượt Xem</th>
                <th>Loại Tin</th>
                <th>Tác vụ</th>
            </tr>
            </thead>
        </table>
    </div>
    <!-- /.container-fluid -->
</div>
<!-- /#page-wrapper -->
@endsection

@section('script')
    <script type="text/javascript" language="javascript">
        var dataTables = getData();
        $(document).ready(function () {
            addData();
            saveData();
            editData();
            deleteData();
            changeLoaitin();
        })

        function getData() {
            var data = $('#tintucDataTables').DataTable({
                "responsive": true,
                "ajax": "{{ route('tintuc.getdata') }}",
                "columns": [
                    {"class": "text-center", "data": "id"},
                    {"class": "text-center", "data": "TieuDe"},
                    /*{"class": "text-center", "data": "TomTat"},*/
                    {
                        "class": "text-center", "data": function (row, type, set, meta) {
                        if (row.Hinh != '') {
                            var url = "upload/tintuc/" + row.Hinh;
                            return "<img src='" + url + "' style='width: 100px' />";
                        }
                        return '';
                    }
                    },
                    {"class": "text-center", "data": "tNoiBat"},
                    {"class": "text-center", "data": "SoLuotXem"},
                    {"class": "text-center", "data": "LoaiTin"},
                    {
                        "class": "text-center",
                        "data": function () {
                            var html = '<a class="btn btn-primary btn-edit">' +
                                    '<i class="fa fa-pencil fa-fw"></i></a>';
                            html += " ";
                            html += '<a class="btn btn-danger btn-delete" ><i class="fa fa-trash-o fa-fw"></i></a>';
                            return html;
                        }
                    },
                ]
            });
            return data;
        }
        function saveData() {
            $(document).on('click', '#tintucSave', function (e) {
                e.preventDefault();
                for (instance in CKEDITOR.instances) {
                    CKEDITOR.instances[instance].updateElement();
                }
                var serialise = $("#tintucSave").serialize();
                var formData = new FormData($('#tintucFormModal')[0]);
                $.ajax({
                    url: "{{ route('tintuc.postsave') }}",
                    method: "POST",
                    data: formData,
                    processData: false,
                    contentType: false,
                    cache: false,
                    success: function (data) {
                        if (data.errors) {
                            $('.alert-danger').html('');
                            $('.alert-danger').fadeIn(2000);
                            $.each(data.errors, function (key, value) {
                                $('.alert-danger').append('<li>' + value + '</li>');
                            });
                            $('.alert-danger').fadeOut(2000);
                        }
                        else {
                            $('.alert-danger').hide();
                            $('#tintucFormModal')[0].reset();
                            $('#tintucModal').modal('hide');
                            $('#tintucDataTables').DataTable().ajax.reload();
                        }
                    }
                })
            })
        }

        function addData() {
            $(document).on('click', '.btn-add', function () {
                $("#tintucModal").find("h4").text("Thêm tin tức");
                $("#tintucModal").modal('show');
                $("input[name='tintucTieuDe']").val('');
                $("textarea[name='tintucTomTat']").val('');
                $("textarea[name='tintucNoiDung']").val('');
                document.getElementById("tintucImagePre").src = '';
                $("input:radio[name='tintucNoiBat'] :selected").val();
            })
        }

        function editData() {
            $(document).on('click', '.btn-edit', function () {
                $("#tintucModal").find("h4").text("Sửa tin tức");
                $("#tintucModal").modal('show');
                var row = $(this).parents('tr');
                var data = dataTables.row(row).data();
                $("input[name='tintucTieuDe']").val(data.TieuDe);
                $("textarea[name='tintucTomTat']").val(data.TomTat);
                $("textarea[name='tintucNoiDung']").val(data.NoiDung);
                $("input:radio[name='tintucNoiBat']:selected").val(data.NoiBat);
                document.getElementById("tintucImagePre").src = "upload/tintuc/" + data.image;
            })
        }

        function deleteData() {
            $(document).on('click', '.btn-delete', function () {
                var row = $(this).parents('tr');
                var data = dataTables.row(row).data();
                var id = data['id'];
                $.ajax({
                    url: "{{ route('tintuc.postdelete') }}",
                    method: "POST",
                    dataType: "json",
                    data: {
                        id: id,
                    },
                    success: function (data) {
                        $('#tintucDataTables').DataTable().ajax.reload();
                    }
                })
            })
        }

        function changeLoaitin() {
            $(document).ready(function () {
                $('#tintucidTheLoai').change(function () {
                    var idTheLoai = $(this).val();
                    $.get("admin/ajax/loaitin/" + idTheLoai, function (data) {
                        $('#tintucidLoaiTin').html(data);
                    });
                });
            });
        }
    </script>
@endsection