<?php

namespace App\Http\Controllers;

use App\tintuc;
use App\User;
use Illuminate\Http\Request;
use App\theloai;
use App\loaitin;
use App\slide;
use Illuminate\Support\Facades\Auth;

class PageController extends Controller
{
    function __construct()
    {
        $theloai = theloai::all();
        view()->share('theloai',$theloai);
    }
    function home()
    {
        $slide = slide::all();
        return view('pages.home',['slide'=>$slide]);
    }
    function test()
    {
        $slide = slide::all();
        return view('pages.test',['slide'=>$slide]);
    }
    function contact()
    {
        return view('pages.contact');
    }
    function about()
    {
        return view('pages.about');
    }
    function loaitin($id)
    {
        $loaitin = loaitin::find($id);
        $tintuc = tintuc::where('idLoaiTin',$id)->paginate(2);
        return view('pages.loaitin',['loaitin' => $loaitin, 'tintuc' => $tintuc]);
    }
    function tintuc($id)
    {
        $tintuc = tintuc::find($id);
        $tinnoibat = tintuc::where('NoiBat',1)->take(3)->get();
        $tinlienquan = tintuc::where('idLoaiTin',$tintuc->idLoaiTin)->take(3)->get();
        return view('pages.tintuc',['tintuc' => $tintuc, 'tinnoibat' => $tinnoibat, 'tinlienquan' => $tinlienquan]);
    }

    function getLogin()
    {
        return view('pages.login');
    }

    function postLogin(Request $rq)
    {
        $this->validate($rq,
            [
                'email'=>'required|min:3|max:50',
                'password' => 'required|min:3|max:50'
            ],
            [
                'email.required' => 'Bạn chưa nhập email',
                'email.min' => 'Bạn nhập email nhỏ hơn 3 kí tự',
                'email.max' => 'Bạn nhập email lớn hơn 50 kí tự',
                'password.required' => 'Bạn chưa nhập password',
                'password.min' => 'Bạn nhập password nhỏ hơn 3 kí tự',
                'password.max' => 'Bạn nhập password lớn hơn 50 kí tự',
            ]
        );
        if(Auth::attempt(['email'=>$rq->email,'password'=>$rq->password]))
        {
            return redirect('home');
        }
        else
        {
            return redirect('login')->with('Error','Email or password incorrect');
        }
    }

    function getLogout()
    {
        Auth::logout();
        return redirect('login');
    }

    function getRegister()
    {
        return view('pages.register');
    }
    function postRegister(Request $rq)
    {
        $this->validate($rq,
            [
                'name' => 'required|min:3|max:50',
                'email'=>'required|min:3|max:50',
                'password' => 'required|min:3|max:50',
                'passwordAgain' => 'required|same:password|min:3|max:50'
            ],

            [
                'name.required' => 'Bạn chưa nhập username',
                'name.min' => 'Bạn nhập username nhỏ hơn 3 kí tự',
                'name.max' => 'Bạn nhập username lớn hơn 50 kí tự',
                'email.required' => 'Bạn chưa nhập email',
                'email.min' => 'Bạn nhập email nhỏ hơn 3 kí tự',
                'email.max' => 'Bạn nhập email lớn hơn 50 kí tự',
                'password.required' => 'Bạn chưa nhập password',
                'password.min' => 'Bạn nhập password nhỏ hơn 3 kí tự',
                'password.max' => 'Bạn nhập password lớn hơn 50 kí tự',
                'passwordAgain.required' => 'Bạn chưa nhập password',
                'passwordAgain.same' => 'Bạn nhập password không trùng khớp',
                'passwordAgain.min' => 'Bạn nhập password nhỏ hơn 3 kí tự',
                'passwordAgain.max' => 'Bạn nhập password lớn hơn 50 kí tự',
            ]
        );
        $user = new User;
        $user->name = $rq->name;
        $user->email= $rq->email;
        $user->image= 'default.jpg';
        $user->password = bcrypt($rq->password);
        $user->level = 2; //user
        $user->save();

        return redirect('login')->with('Notified','Đã đăng kí thành công, vui lòng đăng nhập');
    }

    function getAccount()
    {
        if(Auth::check()) {
            $user = Auth::user();
        }
        return view('pages.account',['user'=>$user]);
    }
    function postAccount(Request $rq)
    {
        $this->validate($rq,
            [
                'password' => 'required',
                'passwordAgain' => 'required|same:password'
            ],
            [
                'password.required' => 'Bạn chưa nhập password',
                'passwordAgain.required' => 'Bạn chưa nhập lại password',
                'passwordAgain.same' => 'Password không trùng khớp'
            ]
        );
        if(Auth::check())
        {
            $user = Auth::user();
            $user->password = bcrypt($rq->password);
            $user->save();
        }
        return redirect('account');
    }
    function getSearch(Request $rq){
        $tukhoa = $rq->tukhoa;
        $tintuc = TinTuc::where('TieuDe','like',"%$tukhoa%")
            ->orWhere('TomTat','like',"%$tukhoa%")
            ->orWhere('NoiDung','like',"%$tukhoa%")->take(5)->paginate(2);
        return view('pages.timkiem',['tintuc'=>$tintuc, 'tukhoa'=>$tukhoa]);
    }
}
