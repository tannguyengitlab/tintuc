<?php

namespace App\Http\Controllers;
namespace App\Http\Events;

use App\tintuc;
use App\loaitin;
use App\theloai;
use App\comment;
use Yajra\Datatables\Datatables;
use Validator;
use Illuminate\Http\Request;

class TinTucController extends Controller
{
    public function getList()
    {
        $theloai = theloai::all();
        $loaitin = loaitin::all();
        return view('admin.tintuc.list', ['theloai' => $theloai, 'loaitin' => $loaitin]);
    }

    public function getData()
    {
        $tintuc = tintuc::all();
        $listloaitin = loaitin::all()->pluck('Ten', 'id');
        foreach ($tintuc as $key => $tt) {
            $idLoaiTin = $tt->idLoaiTin;
            $tintuc[$key]['LoaiTin'] = isset($listloaitin[$idLoaiTin]) ? $listloaitin[$idLoaiTin] : '';

            if ($tt->NoiBat == 1) {
                $tintuc[$key]['tNoiBat'] = "Có";
            } else {
                $tintuc[$key]['tNoiBat'] = "Không";
            }
        }
        return DataTables::of($tintuc)->toJson();
    }

    public function postSave(Request $rq)
    {
        $validator = Validator::make($rq->all(), [
            [
                /*'tintucNoiDung' => 'required',
                'tintucTieuDe' => 'required|unique:tintucs,TieuDe',*/
            ],
            [
                'tintucTieuDe.required' => 'Vui lòng nhập tên tiêu đề',
                'tintucTieuDe.unique' => 'Tiêu đề đã tồn tại',
                'tintucNoiDung.required' => 'Vui lòng nhập nội dung'
            ]
        ]);

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()->all()]);
        }

        if ($rq->tintucId) {
            $tintuc = tintuc::find($rq->tintucId);
            $tintuc->TieuDe = $rq->tintucTieuDe;
            $tintuc->TieuDeKhongDau = changeTitle($rq->tintucTieuDe);
            $tintuc->TomTat = $rq->tintucTomTat;
            $tintuc->NoiDung = $rq->tintucNoiDung;
            $tintuc->SoLuotXem = 0;
            $tintuc->NoiBat = $rq->tintucNoiBat;
            $tintuc->idLoaiTin = $rq->tintucLoaiTin;
            if ($rq->hasFile('tintucImage')) {
                $file = $rq->file('tintucImage');
                $name = $file->getClientOriginalName();
                $namephoto = time() . '_' . $name;
                $file->move('upload/tintuc', $namephoto);
                $tintuc->Hinh = $namephoto;
            } else {
                $tintuc->Hinh = "";
            }
            $tintuc->save();
            return response()->json(['success' => 'Sửa thành công']);
        }
        else{
            $tintuc = new tintuc();
            $tintuc->TieuDe = $rq->tintucTieuDe;
            $tintuc->TieuDeKhongDau = changeTitle($rq->tintucTieuDe);
            $tintuc->TomTat = $rq->tintucTomTat;
            $tintuc->NoiDung = $rq->tintucNoiDung;
            $tintuc->SoLuotXem = 0;
            $tintuc->NoiBat = $rq->tintucNoiBat;
            $tintuc->idLoaiTin = $rq->tintucLoaiTin;
            if ($rq->hasFile('tintucImage')) {
                $file = $rq->file('tintucImage');
                $name = $file->getClientOriginalName();
                $namephoto = time() . '_' . $name;
                $file->move('upload/tintuc', $namephoto);
                $tintuc->Hinh = $namephoto;
            }
            else {
                $tintuc->Hinh = "";
            }
            $tintuc->save();
            return response()->json(['success' => 'Thêm thành công']);
        }
    }

    public function postDelete(Request $rq)
    {
        $tintuc = tintuc::find($rq->id);
        $tintuc->delete();
        return response()->json(['success' => 'Xóa thành công']);
    }

    public function handle(tintuc $tintucs){
        $tintucs->increment('SoLuotXem');
    }
}
