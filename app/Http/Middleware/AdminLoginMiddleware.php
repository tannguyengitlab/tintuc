<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
//use Illuminate\Support\Facades\Auth;

class AdminLoginMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(Auth::check())
        {
            //echo 'da dang nhap'; die;
            $user = Auth::user();
            if($user->level == 1)
                return $next($request);
            else
                return redirect('admin/login');
        }
        else
        {
            //echo 'chua dang nhap'; die;
            return redirect('admin/login');
        }
    }
}
