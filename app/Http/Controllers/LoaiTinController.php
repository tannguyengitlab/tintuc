<?php

namespace App\Http\Controllers;

use App\loaitin;
use App\theloai;
use Yajra\Datatables\Datatables;
use Validator;
use Illuminate\Http\Request;

class LoaiTinController extends Controller
{
    public function getList()
    {
        $theloai = theloai::all();
        return view('admin.loaitin.list', ['theloai' => $theloai]);
    }

    public function getData()
    {
        $loaitin = loaitin::all();
        $listtheloai = theloai::all()->pluck('Ten', 'id');
        foreach ($loaitin as $key => $lt) {
            $idTheLoai = $lt->idTheLoai;
            $loaitin[$key]['TheLoai'] = isset($listtheloai[$idTheLoai]) ? $listtheloai[$idTheLoai] : '';
        }
        return DataTables::of($loaitin)->toJson();
    }


    public function postSave(Request $rq)
    {
        $validator = Validator::make($rq->all(),
            [
                'loaitinName' => 'required',
            ],
            [
                'loaitinName.required' => 'Vui lòng nhập tên loại tin',
            ]);
        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()->all()]);
        }
        if($rq->loaitinId) {
            $loaitin = loaitin::find($rq->loaitinId);
            $loaitin->Ten = $rq->loaitinName;
            $loaitin->TenKhongDau = changeTitle($rq->loaitinName);
            $loaitin->idTheLoai = $rq->idTheLoai;
            $loaitin->save();
            return response()->json(['success' => 'Sửa thành công']);
        }
        else{
            $loaitin = new loaitin();
            $loaitin->Ten = $rq->loaitinName;
            $loaitin->TenKhongDau = changeTitle($rq->loaitinName);
            $loaitin->idTheLoai = $rq->idTheLoai;
            $loaitin->save();
            return response()->json(['success' => 'Thêm thành công']);
        }
    }

    public function postDelete(Request $rq)
    {
        $loaitin = loaitin::find($rq->id);
        $loaitin->delete();
        return response()->json(['success' => 'Xóa thành công']);
    }
}
