<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('admin/login', 'UserController@getLogin');
Route::post('admin/login', 'UserController@postLogin');
Route::get('admin/logout', 'UserController@getLogout');

Route::group(['prefix' => 'admin', 'middleware' => 'adminLogin'], function () {
    Route::get('dashboard', function () {
        return view('admin.dashboard');
    });
    Route::group(['prefix' => 'theloai'], function () {
        Route::get('list', 'TheLoaiController@getList')->name('theloai.getlist');
        Route::get('data', 'TheLoaiController@getData')->name('theloai.getdata');
        Route::post('save', 'TheLoaiController@postSave')->name('theloai.postsave');
        Route::post('delete', 'TheLoaiController@postDelete')->name('theloai.postdelete');
    });
    Route::group(['prefix' => 'loaitin'], function () {
        Route::get('list', 'LoaiTinController@getList')->name('loaitin.getlist');
        Route::get('data', 'LoaiTinController@getData')->name('loaitin.getdata');
        Route::post('save', 'LoaiTinController@postSave')->name('loaitin.postsave');
        Route::post('delete', 'LoaiTinController@postDelete')->name('loaitin.postdelete');
    });
    Route::group(['prefix' => 'tintuc'], function () {
        Route::get('list', 'TinTucController@getList')->name('tintuc.getlist');
        Route::get('data', 'TinTucController@getData')->name('tintuc.getdata');
        Route::post('save', 'TinTucController@postSave')->name('tintuc.postsave');
        Route::post('delete', 'TinTucController@postDelete')->name('tintuc.postdelete');
    });
    Route::group(['prefix' => 'User'], function () {
        Route::get('list', 'UserController@getList')->name('user.getlist');
        Route::get('data', 'UserController@getData')->name('user.getdata');
        Route::post('save', 'UserController@postSave')->name('user.postsave');
        Route::post('delete', 'UserController@postDelete')->name('user.postdelete');
    });
    Route::group(['prefix' => 'slide'], function () {
        Route::get('list', 'SlideController@getList')->name('slide.getlist');
        Route::get('data', 'SlideController@getData')->name('slide.getdata');
        Route::post('save', 'SlideController@postSave')->name('slide.postsave');
        Route::post('delete', 'SlideController@postDelete')->name('slide.postdelete');
    });

    Route::group(['prefix' => 'comment'], function () {
        Route::get('delete/{id}', 'CommentController@getDelete');
    });

    Route::group(['prefix' => 'ajax'], function () {
        Route::get('loaitin/{idTheLoai}', 'AjaxController@getLoaiTin');
    });
});

Route::get('home', 'PageController@home');
Route::get('/', 'PageController@home');
Route::get('contact', 'PageController@contact');
Route::get('about', 'PageController@about');
Route::get('loaitin/{id}/{TenKhongDau}.html', 'PageController@loaitin');
Route::get('tintuc/{id}/{TenKhongDau}.html', 'PageController@tintuc');
Route::get('login', 'PageController@getLogin');
Route::post('login', 'PageController@postLogin');
Route::get('logout', 'PageController@getLogout');
Route::get('register', 'PageController@getRegister');
Route::post('register', 'PageController@postRegister');
Route::group(['prefix' => '', 'middleware' => 'login'], function () {
    Route::post('comment/{id}', 'CommentController@postComment');
    Route::get('account', 'PageController@getAccount');
    Route::post('account', 'PageController@postAccount');
});
Route::get('test', 'PageController@test');

Route::get('timkiem','PageController@getSearch');

