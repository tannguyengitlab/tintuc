@extends('layout.index')

@section('title')
    Tìm kiếm : {{$tukhoa}}
@endsection

@section('content')
    <div class="row main-left">
        @include('layout.menu')
        <?php
            function changecolor($str,$tukhoa)
            {
                return str_replace($tukhoa, "<span style='color:#ff0003;'>$tukhoa</span>",$str);
            }
        ?>

        <div class="col-md-9 ">
            <div class="panel panel-default">
                <div class="panel-heading" style="background-color:#337AB7; color:white;">
                    <h4><b>Tìm kiếm: {{$tukhoa}}</b></h4>
                </div>

                @foreach($tintuc as $tt)
                    <div class="row-item row">
                        <div class="col-md-3">

                            <a href="detail.html">
                                <br>
                                <img width="200px" height="200px" class="img-responsive" src="upload/tintuc/{{$tt->Hinh}}" alt="">
                            </a>
                        </div>

                        <div class="col-md-9">
                            <h3>{!! changecolor($tt->TieuDe,$tukhoa) !!}</h3>
                            <p>{!! changecolor($tt->TomTat,$tukhoa)!!}</p>
                            <a class="btn btn-primary" href="tintuc/{{$tt->id}}/{{$tt->TieuDeKhongDau}}.html">Xem Thêm <span class="glyphicon glyphicon-chevron-right"></span></a>
                        </div>
                        <div class="break"></div>
                    </div>
                    @endforeach

                            <!-- Pagination -->
                    <div style="text-align: center">
                        {{ $tintuc->appends(Request::all())->links() }}
                        {{--{{$tintuc->links()}}--}}
                    </div>
                    <!-- /.row -->

            </div>
        </div>

    </div>
@endsection