@extends('admin.layout.index')

@section('title')
    Danh sách loai tin
@endsection

@section('content')
        <!-- Page Content -->
<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Loại tin
                    <small>Danh sách</small>
                </h1>
            </div>
        </div>

        <div class="col-lg-12">
            <!-- Trigger the modal with a button -->
            <div align="right">
                <button type="button" class="btn btn-add btn-success" data-toggle="modal" id="loaitinAdd"><i
                            class="fa fa-plus "></i>
                </button>
            </div>
        </div>
        <!-- /.button -->
        <div class="modal" tabindex="-1" role="dialog" id="loaitinModal">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <form id="loaitinFormModal">
                        @csrf
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            <h4 class="modal-title" align="center"></h4>
                        </div>
                        <!-- /.modal-header -->
                        <div class="col-lg-12">
                            <br/>
                            <div class="alert alert-danger" style="display:none"></div>
                            <div class="alert alert-success" style="display:none"></div>
                        </div>
                        <!-- /.alert-->
                        <div class="modal-body">
                            <div class="row">
                                <div class="form-group col-md-12">
                                    <input type="hidden" class="form-control" name="loaitinId">
                                    <label>Tên loại tin</label>
                                    <input type="text" class="form-control" name="loaitinName"
                                           placeholder="Nhập tên loại tin">
                                </div>
                                <div class="form-group col-md-12">
                                    <label>Thể loại</label>
                                    <select class="form-control" name="idTheLoai">
                                        @foreach($theloai as $tl)
                                            <option value="{{$tl->id}}">{{$tl->Ten}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <!-- /.modal-body -->
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Đóng
                                </button>
                                <button class="btn btn-success" id="loaitinSave">Lưu</button>
                            </div>
                        </div>
                        <!-- /.modal-footer -->
                    </form>
                    <!-- /.form modal -->
                </div>
                {{--</div>--}}
            </div>
        </div>
        <!-- /.Modal -->
        <table class="table table-striped table-bordered table-hover" id="loaitinDataTables" width="100%">
            <thead>
            <tr align="center">
                <th>ID</th>
                <th>Tên</th>
                <th>Tên Không Dấu</th>
                <th>Thể Loại</th>
                <th>Tác vụ</th>
            </tr>
            </thead>
        </table>
        <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
</div>
<!-- /#page-wrapper -->
@endsection

@section('script')
    <script type="text/javascript" language="javascript">
        var dataTables = getData();
        $(document).ready(function () {
            addData();
            editData();
            saveData();
            deleteData();
        })

        function getData() {
            var data = $('#loaitinDataTables').DataTable({
                "responsive": true,
                "ajax": "{{ route('loaitin.getdata') }}",
                "columns": [
                    {"class": "text-center", "data": "id"},
                    {"class": "text-center", "data": "Ten"},
                    {"class": "text-center", "data": "TenKhongDau"},
                    {"class": "text-center", "data": "TheLoai"},
                    {
                        "class": "text-center",
                        "data": function () {
                            var html = '<a class="btn btn-primary btn-edit">' +
                                    '<i class="fa fa-pencil fa-fw"></i></a>';
                            html += " ";
                            html += '<a class="btn btn-danger btn-delete" ><i class="fa fa-trash-o fa-fw"></i></a>';
                            return html;
                        }
                    },
                ]
            });
            return data;
        }

        function saveData() {
            $(document).on('click', '#loaitinSave', function (e) {
                e.preventDefault();
                var formData = new FormData($('#loaitinFormModal')[0]);
                $.ajax({
                    url: "{{ route('loaitin.postsave') }}",
                    type: "POST",
                    data: formData,
                    processData: false,
                    contentType: false,
                    success: function (data) {
                        if (data.errors) {
                            $('.alert-danger').html('');
                            $('.alert-danger').fadeIn(2000);
                            $.each(data.errors, function (key, value) {
                                $('.alert-danger').append('<li>' + value + '</li>');
                            });
                            $('.alert-danger').fadeOut(2000);
                        }
                        else {
                            $('.alert-danger').hide();
                            $('#loaitinFormModal')[0].reset();
                            $('#loaitinModal').modal('hide');
                            $('#loaitinDataTables').DataTable().ajax.reload();
                        }
                    }
                })
            })
        }

        function addData() {
            $(document).on('click', '.btn-add', function (e) {
                e.preventDefault();
                $("#loaitinModal").find("h4").text("Thêm loại tin");
                $("#loaitinModal").modal('show');
                $("input[name='loaitinId']").val('');
                $("input[name='loaitinName']").val('');
//                $("input[name='loaitinName']").val();
            })
        }
        function editData() {
            $(document).on('click', '.btn-edit', function (e) {
                e.preventDefault();
                $("#loaitinModal").find("h4").text("Sửa loại tin");
                $("#loaitinModal").modal('show');
                var row = $(this).parents('tr');
                var data = dataTables.row(row).data();
                $("input[name='loaitinId']").val(data.id);
                $("input[name='loaitinName']").val(data.Ten);
            })
        }

        function deleteData() {
            $(document).on('click', '.btn-delete', function () {
                var row = $(this).parents('tr');
                var data = dataTables.row(row).data();
                var id = data['id'];
                $.ajax({
                    url: "{{ route('loaitin.postdelete') }}",
                    method: "POST",
                    dataType: "json",
                    data: {
                        id: id,
                    },
                    success: function (data) {
                        $('#loaitinDataTables').DataTable().ajax.reload();
                    }
                })
            })
        }
    </script>
@endsection