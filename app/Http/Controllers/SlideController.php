<?php

namespace App\Http\Controllers;

use App\slide;
use Yajra\Datatables\Datatables;
use Validator;
use Illuminate\Http\Request;

class SlideController extends Controller
{
    public function getList(){
        return view('admin.slide.list');
    }
    public function getData(){
        $slide = slide::all();
        return DataTables::of($slide)->toJson();
    }

    public function postSave(Request $rq){
        $validator = Validator::make($rq->all(), [
            'slideImage' => 'required|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ],
        [
            'slideImage.required' => 'Vui lòng nhập hình ảnh',
            'slideImage.mimes' => 'Chỉ chọn file jpeg, png, jpg, gif, svg',
            'slideImage.max' => 'File tối đa 2MB'
        ]);

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()->all()]);
        }
        if($rq->slideId)
        {
            $slide = slide::find($rq->slideId);
            $slide->Ten = $rq->slideName;
            $slide->NoiDung = $rq->slideContent;
            $slide->Link = $rq->slideLink;
            if ($rq->hasFile('slideImage')) {
                $file = $rq->file('slideImage');
                $name = $file->getClientOriginalName();
                $namephoto = time() . '_' . $name;
                $file->move('upload/slide', $namephoto);
                $slide->Hinh = $namephoto;
            } else {
                $slide->Hinh = "";
            }
            $slide->save();
            return response()->json(['success' => 'Sửa thành công']);
        }
        else
        {
            $slide = new slide();
            $slide->Ten = $rq->slideName;
            $slide->NoiDung = $rq->slideContent;
            $slide->Link = $rq->slideLink;
            if ($rq->hasFile('slideImage')) {
                $file = $rq->file('slideImage');
                $name = $file->getClientOriginalName();
                $namephoto = time() . '_' . $name;
                $file->move('upload/slide', $namephoto);
                $slide->Hinh = $namephoto;
            } else {
                $slide->Hinh = "";
            }
            $slide->save();
            return response()->json(['success' => 'Thêm thành công']);
        }
    }

    public function postDelete(Request $rq)
    {
        $slide = slide::find($rq->id);
        $slide->delete();
        return response()->json(['success' => 'Delete successfully']);
    }
}
