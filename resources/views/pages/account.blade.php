@extends('layout.index')

@section('title')
    Thông tin tài khoản
@endsection

@section('content')
<div class="container">

    <!-- slider -->
    <div class="row carousel-holder">
        <div class="col-md-2">
        </div>
        <div class="col-md-8">
            <div class="panel panel-default">
                <div class="panel-heading">Thông tin tài khoản</div>
                <div class="panel-body">
                    <form>
                        <input type="hidden" name="_token" value="{{csrf_token()}}"/>
                        <div>
                            <label>Tên tài khoản</label>
                            <input readonly type="text" class="form-control" value="{{$user->name}}" name="name" aria-describedby="basic-addon1">
                        </div>
                        <br>
                        <div>
                            <label>Email</label>
                            <input type="email" class="form-control" value="{{$user->email}}" name="email" aria-describedby="basic-addon1"
                                   readonly
                            >
                        </div>
                        <br>
                        <div>
                            <input type="checkbox" id="changePassword" class="" name="changePassword">
                            <label>Đổi mật khẩu</label>
                            <input disabled="" type="password" class="form-control password" name="password" aria-describedby="basic-addon1">
                        </div>
                        <br>
                        <div>
                            <label>Nhập lại mật khẩu</label>
                            <input disabled="" type="password" class="form-control password" name="passwordAgain" aria-describedby="basic-addon1">
                        </div>
                        <br>
                        <button type="submit" class="btn btn-default">Sửa
                        </button>

                    </form>
                </div>
            </div>
        </div>
        <div class="col-md-2">
        </div>
    </div>
    <!-- end slide -->
</div>
@endsection

{{--@section('script')
    <script>
        $(document).ready(function(){
            $('#checkpassword').change(function(){
                if($(this).is(":checked"))
                {
                    alert("da tich")
                    $(".password").removeAttr('disabled')
                }
                else
                {
                    alert("Chua tich")
                    $(".password").attr('disabled')
                }
            })
        })
    </script>
@endsection--}}

@section('script')
    <script>
        $(document).ready(function()
        {
            $('#changePassword').change(function()
            {
                if($(this).is(":checked"))
                {
                    //alert("Da tich");
                    $(".password").removeAttr('disabled');
                }
                else
                {
                    //alert('Chua tich')
                    $(".password").attr('disabled','');
                }
            });
        });
    </script>
@endsection