@extends('layout.index')
@section('content')
    <div class="">
        <form action="" method="POST">
            <div class="form-group">
                <label>Nhập tên</label>
                <input class="form-control" name="ten"/>
            </div>
            <button id="btn-submit" type="button" class="btn btn-default">Thêm</button>
            <button id="btn-reset" type="reset" class="btn btn-default">Reset</button>
        </form>
    </div>
    <div align="central" class="thongbao" hidden>
        <h1>Hello</h1>
    </div>
@endsection
@section('script')
    <script>
        $(document).ready(function () {
            $('#btn-submit').click(function () {
                $('.thongbao').fadeIn('slow').fadeOut('slow')
            });
        });
    </script>
@endsection