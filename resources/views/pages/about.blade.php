@extends('layout.index')

@section('title')
    Giới thiệu
@endsection

@section('content')
        <!-- Page Content -->
<div class="container">
    <div class="row main-left">
        @include('layout.menu')

        <div class="col-md-9">
            <div class="panel panel-default">
                <div class="panel-heading" style="background-color:#337AB7; color:white;">
                    <h2 style="margin-top:0px; margin-bottom:0px;">Giới thiệu</h2>
                </div>

                <div class="panel-body">
                    <!-- item -->
                    <p>
                        Tan Nguyen - Laravel
                    </p>

                </div>
            </div>
        </div>
    </div>
    <!-- /.row -->
</div>
@endsection