<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
//use Illuminate\Support\Facades\Auth;

class LoginMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(Auth::check())
        {
            //echo 'da dang nhap'; die;
            $user = Auth::user();
            if($user)
                return $next($request);
            else
                return redirect('login');
        }
        else
        {
            //echo 'chua dang nhap'; die;
            return redirect('login');
        }
        //return $next($request);
    }
}
