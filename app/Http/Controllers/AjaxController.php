<?php

namespace App\Http\Controllers;

use App\theloai;
use App\loaitin;
use Illuminate\Http\Request;

class AjaxController extends Controller
{
    public function getLoaiTin($idTheLoai)
    {
        $loaitin = loaitin::where('idTheLoai',$idTheLoai)->get();
        foreach($loaitin as $lt)
        {
            echo "<option value='".$lt->id."'>$lt->Ten</option>";
        }
    }
}
